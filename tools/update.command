ABSPATH=$(cd "$(dirname "$0")"; pwd -P)
cd "${ABSPATH}"

if which npm > /dev/null; then
      echo "npm is installed, ok..."
else
      echo "npm is NOT installed, stopping script..."
      exit
fi

if which yarn > /dev/null; then
      echo "yarn is installed, ok..."
else
      echo "yarn is NOT installed, stopping script..."
      exit
fi

if which node > /dev/null; then
      echo "node is installed, ok..."
else
      echo "node is NOT installed, stopping script..."
      exit
fi

node -v
npm -v

echo "===================================================================="
echo "Going to repo root directory"
echo "********"
cd ..

echo ""
echo "===================================================================="
echo "Activating virtualenv"
echo "********"
cd backend
source ./venv/bin/activate
cd ..

echo ""
echo "=================================="
echo "Is virtualenv activated?"
echo "********"
if [ -z "$VIRTUAL_ENV" ]
then
      echo "Virtualenv is NOT activated! You have to fix this! Stopping script..."
      echo "Pro tip: run init script before this script"
      exit
else
      echo "Virtualenv is activated!"
fi

echo ""
echo "===================================================================="
echo "On Branch"
echo "********"
git rev-parse --abbrev-ref HEAD

echo ""
echo "===================================================================="
echo "Git status"
echo "********"
git status
# git checkout .
# git clean -fd

echo ""
echo "===================================================================="
echo "Going to front directory"
echo "********"
cd front
pwd
# switch to actual version of node and npm
nvm install 16.16.0
npm i

echo ""
echo "===================================================================="
echo "Removing last build files"
echo "********"

if [[ -d ./build ]]
then
      rm -rf ./build/* 
else
      echo "Directory build does not exists! (yet)"  
fi

echo ""
echo "===================================================================="
echo "What in build directory after cleaning"
echo "********"
ls -la build

echo ""
echo "===================================================================="
echo "Building prod build"
echo "********"
npm run build

echo ""
echo "===================================================================="
pwd
echo "Did build even created?"
if [[ -d ./build ]]
then
      echo "Ok build exists"  
else
      echo "Could not create build! Check out npm errors."  
      exit
fi

echo ""
echo "===================================================================="
echo "What in build directory after prod build"
echo "********"
ls -la build

echo ""
echo "===================================================================="
echo "Going to backend directory"
echo "********"
cd ..
cd backend

echo ""
echo "===================================================================="
echo "Migrating"
echo "********"
python3 manage.py migrate

echo ""
echo "===================================================================="
echo "Creating staticfiles directory just in case"
echo "********"
mkdir ./staticfiles

echo ""
echo "===================================================================="
echo "What in staticfiles directory currently"
echo "********"

ls -la ./staticfiles/

echo ""
echo "===================================================================="
echo "Removing files inside staticfiles directory"
echo "********"
rm -rf ./staticfiles/*

echo ""
echo "===================================================================="
echo "Removing files inside staticfiles_collected directory"
echo "********"
rm -rf ./staticfiles_collected/*

echo ""
echo "===================================================================="
echo "What in staticfiles directory after clearing"
echo "********"
ls -la ./staticfiles/

echo ""
echo "===================================================================="
echo "What in staticfiles_collected directory after clearing"
echo "********"
ls -la ./staticfiles_collected/

echo ""
echo "===================================================================="
echo "Collecting static"
echo "********"
python3 manage.py collectstatic --noinput

echo ""
echo "===================================================================="
echo "What in staticfiles_collected directory after collecting"
echo "********"
ls -la ./staticfiles_collected/

echo ""
echo "===================================================================="
echo "Moving some files"
echo "********"
pwd

cp -r ./staticfiles_collected/* ./staticfiles
cp -r ./../front/build/* ./staticfiles
mkdir ./staticfiles/static/admin

echo ""
echo "===================================================================="
echo "Moving admin to static"
echo "********"
mkdir ./staticfiles/static/admin
cp -r ./staticfiles/admin/* ./staticfiles/static/admin

echo ""
echo "===================================================================="
echo "Creating templates directory just in case"
echo "********"
mkdir ./templates

echo ""
echo "===================================================================="
echo "What in templates directory currently"
echo "********"
ls -la ./templates

echo ""
echo "===================================================================="
echo "Removing contents of templates directory"
echo "********"
rm ./templates/*

echo ""
echo "===================================================================="
echo "What in templates directory after cleaning"
echo "********"
ls -la ./templates

echo ""
echo "===================================================================="
echo "Moving new index.html into templates"
echo "********"
cp -r ./staticfiles/index.html ./templates/index.html

echo ""
echo "===================================================================="
echo "What in templates directory currently"
echo "********"
ls -la ./templates

echo ""
echo "=================================="
echo "Restarting backend"
echo ""

sudo systemctl restart gunicorn

echo ""
echo "=================================="
echo "Done"
echo ""
