import React, { useState, useEffect } from "react";

import { useNavigate } from "react-router-dom";
import axios from 'axios';

import './MainPage.css';
import MainLogo from "../components/ui/MainLogo";

export const baseUrl = process.env.REACT_APP_BACKEND_API_URL;

export default function MainPage() {
  const navigate = useNavigate();

  const [countries, setCountries] = useState([])
  const [cities, setCities] = useState([])
  const [combinedData, setCombinedData] = useState([])

  const handleNavigate = async (country, city) => {
    navigate(`/map?country=${country}&city=${city}`)
  }

  const getCities = () => {
    if (!countries.length) return;

    axios.get(`${baseUrl}/api/cities/`).then((data) => {
      setCities(data.data)
    }).catch(error => {
      console.info('Error', error)
    })
  }

  const getCountries = () => {
    axios.get(`${baseUrl}/api/countries/`).then((data) => {
      setCountries(data.data)
    }).catch(error => {
      console.info('Error', error)
    })
  }

  useEffect(() => {
    if (!countries.length || !cities.length) return;

    cities.map(item => {
      item.countryData = countries.find(item2 => item2.id === item.country)
      item.countryName = countries.find(item2 => item2.id === item.country).name
    })

    setCombinedData(cities)
  }, [cities])

  useEffect(() => {
    getCities()
  }, [countries])

  useEffect(() => {
    getCountries()
  }, [])

  return (
    <div className="main-bg">
      <div className="left-pane d-flex flex-column">
        <MainLogo className="mb-4" />
        <div className="center-items">
          <div className="d-flex flex-column">
            <h1 className="mb-6 hero-text-2">Пора освоиться в новой стране</h1>
            <div className="d-flex flex-row justify-center">
              <div
                className="button hero-button hero-button-1 w-half pv-20-percent mr-5 hero-button-text"
                onClick={() => handleNavigate(combinedData[0]?.countryName, combinedData[0]?.name)}>
                {combinedData[0]?.name}<br />
                ({combinedData[0]?.countryName})
              </div>
              <div
                className="button hero-button hero-button-2 w-half pv-20-percent hero-button-text"
                onClick={() => handleNavigate(combinedData[1]?.countryName, combinedData[1]?.name)}>
                {combinedData[1]?.name}<br />
                ({combinedData[1]?.countryName})
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
