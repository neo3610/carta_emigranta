import React, { useState, useEffect } from "react";

import axios from 'axios';
import { useNavigate } from "react-router-dom";

import './ListModeModalPage.css';

import { ReactComponent as BurgerCloseIcon } from '../assets/icons/burger-close.svg';

import Search from "../components/ui/Search";
import ShowItem from "../components/ShowItem";
import ListItemsHorizontal from "../components/ListItemsHorizontal";
import CategoryIcon from "../components/ui/CategoryIcon";

export const baseUrl = process.env.REACT_APP_BACKEND_API_URL;

function ListModeModalPage(props) {
    const { city, country, closeListModeModal, menuMapItems, clickPlace, clickMenuItem } = props;
    const navigate = useNavigate();

    const [_menuMapItems, set_menuMapItems] = useState([])

    const handleNavigate = async (country, city) => {
        navigate('/');
        setTimeout(() => {
            navigate(`/map?country=${country}&city=${city}`, { replace: true })
        }, 20)
    }

    useEffect(() => {
        set_menuMapItems(menuMapItems)
    }, [menuMapItems])

    const setSearchRes = (data) => {
        set_menuMapItems(data)
    }

    const [shownItems, setShownItems] = useState([])
    const [placeholderShownItems, setPlaceholderShownItems] = useState([])
    const [showInfoItem, setshowInfoItem] = useState({})

    const [countries, setCountries] = useState([])
    const [cities, setCities] = useState([])
    const [combinedData, setCombinedData] = useState([])

    const getCountries = () => {
        axios.get(`${baseUrl}/api/countries/`).then((data) => {
            setCountries(data.data)
        }).catch(error => {
            console.info('Error', error)
        })
    }

    const getCities = () => {
        if (!countries.length) return;

        axios.get(`${baseUrl}/api/cities/`).then((data) => {
            setCities(data.data)
        }).catch(error => {
            console.info('Error', error)
        })
    }

    useEffect(() => {
        if (!countries.length || !cities.length) return;

        cities.map(item => {
            item.countryData = countries.find(item2 => item2.id === item.country)
            item.countryName = countries.find(item2 => item2.id === item.country).name
        })

        setCombinedData(cities)
    }, [cities])

    useEffect(() => {
        getCities()
    }, [countries])

    useEffect(() => {
        getCountries()
    }, [])

    useEffect(() => {
        const _tempItems = []

        _menuMapItems.map(item => {
            item.sub_category.map(item2 => {
                item2.point_objects.map(item3 => {
                    _tempItems.push(item3)
                })
            })
        })

        setPlaceholderShownItems(_tempItems.splice(0, 10))
    }, []);

    const handleChange = (e) => {
        const cityName = e.target.value
        const data = combinedData.find(item => item.name === cityName)

        handleNavigate(data.countryName, data.name)
    }

    return (
        <div className="list-mode-modal-wrapper bg-blur p-3 pr-5 pl-5">
            <div className="d-flex justify-between mb-4 align-items-center">

                <p></p>

                <BurgerCloseIcon onClick={() => closeListModeModal()} className="close-list-mode-modal cursor-pointer" />
            </div>
            <div className="d-flex align-items-center flex-column">
                <a href="" className="list-mode-toggle mb-4" onClick={(e) => { e.preventDefault(); closeListModeModal() }}>Обратно на карту</a>
            
            </div>

            <ListItemsHorizontal
                menuMapItems={_menuMapItems}
                clickPlace={clickPlace}
                clickMenuItem={(item) => {
                    setshowInfoItem({})
                    setShownItems(item)
                    clickMenuItem(item)
                }}
            />

            <div className="mb-4"></div>

            <div className="show-wrapper">
                <div className="show d-flex justify-between p-4 flex-column-mobile">
                    <div className="flex-1 w-half-desktop text-left mr-4-desktop mb-3">
                        <div className="height-scroll-container">
                            <div>
                                {!shownItems?.point_objects ?
                                    placeholderShownItems?.map((item, k) => {
                                        return (
                                            <div
                                                key={k}
                                                onClick={() => {
                                                    setshowInfoItem(item);
                                                    clickPlace(item)
                                                }}
                                                className={
                                                    (showInfoItem?.id === item?.id ? "highlighted-left-menu-item " : "") + 'left-menu-item cursor-pointer text-underline text-hover-underline d-flex align-items-center mb-1'
                                                }
                                            >
                                                <span className="mr-2 d-flex align-items-cente">
                                                    {CategoryIcon(item.categoryId)}
                                                </span>
                                                <span className="text-left">
                                                    {`${item.combinedCategory} / ${item.name}}`}
                                                </span>
                                            </div>
                                        )
                                    })

                                    : ""}

                                {shownItems.point_objects?.map((item, k) => {
                                    return (
                                        <div
                                            key={k}
                                            onClick={() => {
                                                setshowInfoItem(item);
                                                clickPlace(item)
                                            }}
                                            className={
                                                (showInfoItem?.id === item?.id ? "highlighted-left-menu-item " : "") + 'left-menu-item cursor-pointer text-underline text-hover-underline d-flex align-items-center mb-1'
                                            }
                                        >
                                            <span className="mr-2 d-flex align-items-cente">
                                                {CategoryIcon(item.categoryId)}
                                            </span>
                                            <span className="text-left">
                                                {`${shownItems.name} / ${item.name}`}
                                            </span>
                                        </div>
                                    )
                                })}
                            </div>
                        </div>


                    </div>
                    <div className="flex-1 w-half-desktop overflow-word">
                        <>
                            <ShowItem
                                showInfoItem={showInfoItem}
                                defaultContent="Выберите пункт"
                                buttonTitle="Показать на карте"
                                onButtonClick={(e) => {
                                    closeListModeModal();
                                    e.preventDefault();
                                }}
                            />
                        </>
                    </div>
                </div>
            </div>
        </div >
    )
}

export default ListModeModalPage;
