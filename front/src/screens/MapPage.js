import React, { useState, useEffect } from "react";
import { YMaps, Map, Placemark } from '@pbe/react-yandex-maps';
import axios from 'axios';
import { useNavigate } from "react-router-dom";
import { useSearchParams } from "react-router-dom";

import MainLogo from "../components/ui/MainLogo";
import AddObjectButton from "../components/AddObject/AddObjectButton";
import AddObjectModal from "../components/AddObject/AddObjectModal";

import ListItems from "../components/ListItems";
import ShowItem from "../components/ShowItem";

import ListModeModalPage from "./ListModeModalPage";

import { ReactComponent as BurgerOpenIcon } from '../assets/icons/burger-open.svg';
import { ReactComponent as BurgerCloseIcon } from '../assets/icons/burger-close.svg';

import './MapPage.css';

import { citiesCenters } from '../config'

export const baseUrl = process.env.REACT_APP_BACKEND_API_URL;

export default function MapPage() {
  const navigate = useNavigate();

  const handleNavigate = async (country, city) => {
    navigate('/');
    setTimeout(() => {
      navigate(`/map?country=${country}&city=${city}`, { replace: true })
    }, 20)
  }

  const setSearchRes = (data) => {
    setMenuItems(data)
  }

  let [searchParams, setSearchParams] = useSearchParams();

  const [countries, setCountries] = useState([])
  const [cities, setCities] = useState([])
  const [cityId, setCityId] = useState(null)
  const [combinedData, setCombinedData] = useState([])

  const country = searchParams.get("country")
  const city = searchParams.get("city")

  const defaultState = {
    center: citiesCenters.warsaw,
    zoom: 10,
  };

  const getCountries = () => {
    axios.get(`${baseUrl}/api/countries/`).then((data) => {
      setCountries(data.data)

      const countryData = data.data.find(item => item.name === country)
      setcurrentMapData(
        {
          center: [countryData.map_x, countryData.map_y],
          zoom: 16
        }
      )
    }).catch(error => {
      console.info('Error', error)
    })
  }

  const [menuMapItems, setMenuMapItems] = useState([])
  const [shownMapItems, setShownMapItems] = useState([])
  const [showInfoItem, setShowInfoItem] = useState({})
  const [isMobileMenuOpened, setIsMobileMenuOpened] = useState(false)
  const [isListMode, setIsListMode] = useState(false)

  const [currentMapData, setcurrentMapData] = useState({
    center: citiesCenters.warsaw,
    zoom: 10
  });

  const calcCityId = () => {
    cities.map(item => {
      if(item.name === city){
        setCityId(item.id)
      }
    })
  }

  const getCities = () => {
    if (!countries.length) return;

    axios.get(`${baseUrl}/api/cities/`).then((data) => {
      setCities(data.data)
    }).catch(error => {
      console.info('Error', error)
    })
  }

  useEffect(() => {
    if (!countries.length || !cities.length) return;

    cities.map(item => {
      item.countryData = countries.find(item2 => item2.id === item.country)
      item.countryName = countries.find(item2 => item2.id === item.country).name
    })

    setCombinedData(cities)
    calcCityId()
  }, [cities])

  useEffect(() => {
    getCities()
  }, [countries])

  useEffect(() => {
    getCountries()
  }, [])

  const toggleMenu = () => {
    setIsMobileMenuOpened(!isMobileMenuOpened)
  }

  const clickMenuItem = (item) => {
    setShowInfoItem({})

    setShownMapItems(item.point_objects)
    const firstItem = item.point_objects[0];

    setcurrentMapData(
      {
        center: [firstItem.map_x, firstItem.map_y],
        zoom: 16
      }
    )
  }

  const clickPlace = (item, doNotSetMapData = false) => {
    setShowInfoItem(item)

    if (doNotSetMapData) return;
    setcurrentMapData(
      {
        center: [item.map_x, item.map_y],
        zoom: 16
      }
    )
  }

  const clickMap = () => {
    setShowInfoItem({})
  }

  const clickPoint = (item) => {
    setcurrentMapData(
      {
        center: [item.map_x, item.map_y],
        zoom: 16
      }
    )
  }

  const handleChange = (e) => {
    const cityName = e.target.value
    const data = combinedData.find(item => item.name === cityName)

    handleNavigate(data.countryName, data.name)
  }

  const setMenuItems = (menuItems) => {
    const mapItemsFlattened = []

    /* Убираем дубликаты меню - фиксим глюк бэка */
    const menuItemsNoDuplicates = []
    const menuItemsNoDuplicatesIds = []
    menuItems.map(item => {
      if (!menuItemsNoDuplicatesIds.includes(item.id)) {
        menuItemsNoDuplicatesIds.push(item.id)
        menuItemsNoDuplicates.push(item)
      }
    })

    const _menuItems = menuItemsNoDuplicates.map(item => {
      const category = item.name
      const categoryId = item.id

      item.sub_category.map(item2 => {

        const subCategory = item2.name
        item2.categoryId = categoryId;
        item2.point_objects.map(item3 => {

          item3.subCategory = subCategory;
          item3.category = category;
          item3.categoryId = categoryId;
          item3.combinedCategory = `${category} / ${subCategory}`

          mapItemsFlattened.push({ ...item3 })

          return item3;
        })

        return item2;
      })

      const arr = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

      let counter1 = 4000;
      let counter2 = 4000;
      // arr.forEach(el => {
      //   item.sub_category.push(
      //     {
      //       "id": counter1++, "name": "Пример", "point_objects": [
      //         { "id": counter2++, "super_id": 21, "name": "Pola Polish for Foreigners", "address": "00-251, Senatorska 7, Warszawa", "telephone": "605887721", "website": "http://www.pola.edu.pl/", "comment": "Курсы польского языка", "map_x": 52.24674, "map_y": 21.01274, "is_modereted": false, "city": null, "subCategory": "Языковые курсы", "category": "Образование", "categoryId": 5, "combinedCategory": "Образование / Языковые курсы" },
      //         { "id": counter2++, "super_id": 22, "name": "Szkoła Języka Polskiego Together", "address": "Mariensztat 8, 00-302 Warszawa", "telephone": "221115064", "website": "https://together-school.pl/", "comment": "Курсы польского языка", "map_x": 52.24639, "map_y": 21.01826, "is_modereted": false, "city": null, "subCategory": "Языковые курсы", "category": "Образование", "categoryId": 5, "combinedCategory": "Образование / Языковые курсы" },
      //         { "id": counter2++, "super_id": 23, "name": "Edu & More - Polish Language School for Foreigners", "address": "Nowogrodzka 44, 00-695 Warszawa", "telephone": "226221441", "website": "https://polishonlinenow.com/", "comment": "Польский для иностранцев", "map_x": 52.22843, "map_y": 21.00938, "is_modereted": false, "city": null, "subCategory": "Языковые курсы", "category": "Образование", "categoryId": 5, "combinedCategory": "Образование / Языковые курсы" }], "categoryId": 5
      //     }
      //   )
      // })

      return item;
    })

    setMenuMapItems(_menuItems)
    setShownMapItems(mapItemsFlattened)
  }

  useEffect(() => {
    axios.get(`${baseUrl}/api/objects_on_map/?country=${country}&city=${city}`)
      .then(res => {
        const menuItems = res.data
        setMenuItems(menuItems)

      })
  }, []);

  return (
    <>
      {!isListMode ? (
        <div className="top-menu bg-blur pr-5 pl-5">
          <MainLogo className="is-hidden-mobile" />

          <div className="select select-country">
            <select defaultValue="none" onChange={handleChange}>
              <option value="none" disabled>Выбор города</option>
              <option value={combinedData[0]?.name}>{combinedData[0]?.name}</option>
              <option value={combinedData[1]?.name}>{combinedData[1]?.name}</option>
            </select>
          </div>

          <p></p>

          {isMobileMenuOpened ? (
            <BurgerCloseIcon onClick={toggleMenu} className="mobile-menu-toggle" />
          ) : (
            <BurgerOpenIcon onClick={toggleMenu} className="mobile-menu-toggle" />
          )}
        </div>
      ) : null}

      <div className="map-wrapper">
        <YMaps>
          <Map onClick={clickMap} height="100%" width="100%" state={currentMapData} defaultState={defaultState}>
            {shownMapItems.map((item, i) => (
              <Placemark onClick={() => clickPlace(item, true)} key={i} geometry={[item.map_x, item.map_y]} />
            ))}
          </Map>
        </YMaps>

        <ShowItem
          showInfoItem={showInfoItem}
          defaultContent="Выберите обьект на карте"
          onlyMobile
        />

        {!isListMode ? (
          <div className={"right-pane bg-blur " + (isMobileMenuOpened ? "menu-is-opened" : "")}>

            <div className="d-flex justify-between mb-2 mt-2">
              <a href="" onClick={(e) => { e.preventDefault(); setIsListMode(!isListMode) }} className="list-mode-toggle mb-4">Показать списком</a>

              <AddObjectButton triggerButton={
                <a href="" onClick={(e) => { e.preventDefault(); }} className="list-mode-toggle mb-4 hide-on-desktop">Добавить объект</a>
              } />
            </div>

            <ListItems
              menuMapItems={menuMapItems}
              clickPlace={clickPlace}
              clickMenuItem={clickMenuItem}
              clickPoint={clickPoint}
              className="mb-2"
            />

            <ShowItem
              showInfoItem={showInfoItem}
              defaultContent="Выберите обьект на карте"
              onlyDesktop
            />
          </div>
        ) : null}
      </div>

      <AddObjectButton />

      {isListMode ? (
        <ListModeModalPage
          menuMapItems={menuMapItems}
          clickPlace={clickPlace}
          clickMenuItem={clickMenuItem}
          closeListModeModal={() => setIsListMode(false)}
          city={city}
          country={country}
        />
      ) : null}

      <AddObjectModal cityId={cityId} />

    </>
  );
}
