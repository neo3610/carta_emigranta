import React from "react";

import { Routes, Route } from "react-router-dom";

import MainPage from "./screens/MainPage";
import MapPage from "./screens/MapPage";
import NotFound from "./screens/NotFound";

import RootLayout from "./RootLayout";

const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<RootLayout />}>
        <Route index={true} element={<MainPage />} />
        <Route path="map" element={<MapPage />} />
        <Route path="*" element={<NotFound />} />
      </Route>
    </Routes>
  );
};

export default AppRoutes;
