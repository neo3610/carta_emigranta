import { HashRouter } from "react-router-dom";
import AppRoutes from "./AppRoutes";

import './assets/css/main.css';
import './assets/css/helpers.css';

function App() {
  return (
    <HashRouter>
      <AppRoutes />
    </HashRouter>
  );
}

export default App;
