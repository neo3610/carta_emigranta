import React, { useState, useEffect } from "react";

import './ListItemsHorizontal.css';
import { ReactComponent as ArrowDownIcon } from '../assets/icons/arrow-down.svg';
import { ReactComponent as ArrowUpIcon } from '../assets/icons/arrow-up.svg';
import { ReactComponent as DashIcon } from '../assets/icons/dash.svg';

import CategoryIcon from "./ui/CategoryIcon";

function ListItems(props) {
    const { menuMapItems, clickMenuItem } = props;
    const [toggleItems, setToggleItems] = useState({})

    useEffect(() => {
        const toggles = {}
        menuMapItems.map(item => {
            toggles[item.id] = false
        })
        setToggleItems(toggles)
    }, [menuMapItems]);


    const toggleShow = (id) => {
        const _toggleItems = Object.values(toggleItems).map(() => false)
        setToggleItems({ ..._toggleItems, [id]: !toggleItems[id] })
    }

    const _clickMenuItem = (item) => {
        const _toggleItems = Object.values(toggleItems).map(() => false)
        setToggleItems(_toggleItems)

        clickMenuItem(item)
    }

    return (
        <div className="list-items-horizontal">
            <div>
                <ul className="list-items justify-center-desktop">
                    {menuMapItems.map((item, i) => {
                        if (!item?.sub_category) return;

                        return (
                            <li className={"top-level-item z-index-1 " + (toggleItems[item.id] ? "open" : "")} key={i}>
                                <p className="cursor-pointer mb-2" onClick={() => toggleShow(item.id)}>
                                    <span className="mr-2">
                                        {CategoryIcon(item.id)}
                                    </span>
                                    <span className="mr-2">{item.name}</span>
                                    {toggleItems[item.id] ? (
                                        <ArrowUpIcon />
                                    ) : (
                                        <ArrowDownIcon />
                                    )}
                                </p>

                                <ul className="second-level-items">
                                    {item.sub_category.map((item2, j) => {
                                        return (
                                            <li key={j} className="second-level-item" key={j}>
                                                <span onClick={() => _clickMenuItem(item2)}>
                                                    <DashIcon className="mr-2" />
                                                    {item2.name}</span>
                                            </li>
                                        )
                                    })}
                                </ul>
                            </li>
                        )
                    })}
                </ul>
            </div>
        </div>
    );
}

export default ListItems;
