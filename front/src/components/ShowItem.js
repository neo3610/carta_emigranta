import React from "react";

import { is_url } from "../lib/misc";

import CategoryIcon from "./ui/CategoryIcon";

function ShowItem(props) {
    const {
        showInfoItem,
        onlyMobile,
        onlyDesktop,
        buttonTitle,
        onButtonClick
    } = props;

    if (!showInfoItem?.name) return "";

    return (
        <div className={"show-item " + (onlyMobile ? "only-mobile " : "") + (onlyDesktop ? "only-desktop" : "")}>
            <h2 className="is-size-5">
                <span className="mr-2 icon-wrapper">
                    {CategoryIcon(showInfoItem.categoryId)}
                </span>
                <span>
                    {showInfoItem.combinedCategory}
                </span>
            </h2>

            <p className="is-italic is-size-5">{showInfoItem.name}</p>
            <p className="mb-1">{showInfoItem.address}</p>
            <p className="mb-1">
                <a href={"tel:" + showInfoItem.telephone}>
                    {showInfoItem.telephone}
                </a>
            </p>

            <p className="mb-1">{
                is_url(showInfoItem.website) ? (
                    <a target="_blank" href={showInfoItem.website}>{showInfoItem.website}</a>
                ) : showInfoItem.website
            }</p>

            <p className="is-size-7 mb-1">
                {showInfoItem.comment}
            </p>

            {
                buttonTitle && showInfoItem?.map_x ? (
                    <div className="mb-4 mt-4">
                        <a
                            href=""
                            className="list-mode-toggle"
                            onClick={onButtonClick}>{buttonTitle}
                        </a>
                    </div>
                ) : ""
            }
        </div >
    );
}

export default ShowItem;
