import React, { useState, useEffect } from "react";

import { ReactComponent as FindReplaceIcon } from '../../assets/icons/find-replace-line.svg';
import axios from "axios";

export const baseUrl = process.env.REACT_APP_BACKEND_API_URL;

function Search(props) {
    const [value, setValue] = useState("")

    const { city, country, setData } = props;

    useEffect(() => {
        if (!value) return;
        axios.get(`${baseUrl}/api/objects_on_map/?city=${city}&search=${value}`).then((data) => {
            if (!data.data.length) return;
            setData(data.data)
        })
    }, [value])

    return (
        <p className="control has-icons-left mb-3">
            <input onChange={(e) => {
                setValue(e.target.value)
            }} className="input search-input" type="text" placeholder="Что найти?" />
            <span className="icon is-small is-left">
                <FindReplaceIcon />
            </span>
        </p>
    )
}

export default Search;
