import { ReactComponent as CarbonCloudIcon } from '../../assets/icons/carbon-cloud-satellite-services.svg';
import { ReactComponent as BeautyIcon } from '../../assets/icons/map_beauty-salon.svg';
import { ReactComponent as MedicineIcon } from '../../assets/icons/healthicons_medicines-outline.svg';
import { ReactComponent as EducationIcon } from '../../assets/icons/cil_education.svg';
import { ReactComponent as CultureIcon } from '../../assets/icons/healthicons_agriculture-outline.svg';

const icons = {
    1: <BeautyIcon />,
    2: <CultureIcon />,
    3: <CarbonCloudIcon />,
    4: <MedicineIcon />,
    5: <EducationIcon />,
    6: <EducationIcon />,
}

const CategoryIcon = (id) => {
    if (id in icons) return icons[id]
    return icons[3]
}

export default CategoryIcon;
