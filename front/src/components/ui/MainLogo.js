import React from "react";

import './MainLogo.css';

function MainLogo(props) {
    return (
        <div className={"logo " + (props.className ? props.className : "")}>
            <p className="logo-text">Карта эмигранта</p>
        </div>
    )
}

export default MainLogo;
