import React, { useState, useEffect } from "react";
import axios from "axios";

import { ReactComponent as BurgerCloseIcon } from '../../assets/icons/burger-close.svg';
import { eventBus } from "../../lib/eventBus";
import { getCookie } from "../../lib/misc"

import toast from 'react-hot-toast';

import { baseUrl } from '../../config';

import './AddObjectModal.css';

function AddObjectModal(props) {
    const [isModalShown, setIsModalShown] = useState(false)
    const { cityId } = props;

    const defaultState = {
        name: '',
        address: '',
        telephone: '',
        website: '',
        comment: '',
        map_x: '',
        map_y: '',
        storage_version: '1.0.1',
        city: ''
    }

    const [formValue, setformValue] = useState(defaultState);

    const handleChange = (event) => {
        setformValue({
            ...formValue,
            [event.target.name]: event.target.value
        });
        localStorage.setItem('formValue', JSON.stringify(formValue));
    }

    useEffect(() => {
        if (!cityId) return;
        setformValue({
            ...formValue,
            city: cityId
        });
    }, [cityId]);

    useEffect(() => {
        eventBus.on("showModalAddObject", () => {
            showModal();
        });

        let lcstrFormValue = localStorage.getItem('formValue');
        if (lcstrFormValue) {
            setformValue({ ...formValue, ...JSON.parse(lcstrFormValue) })
        }
    }, []);

    const sendObject = () => {
        const _formValue = {}

        Object.entries(formValue).map(item => {
            if (item[1]) _formValue[item[0]] = item[1]
        })

        const options = {
            headers:
            {
                "X-CSRFToken": getCookie('csrftoken')
            }
        }

        axios.post(`${baseUrl}/api/add_objects_on_map/`, _formValue, options).then((resp) => {
            if (resp?.data?.address) {
                localStorage.removeItem("formValue");
                setformValue(defaultState)

                toast.success('Спасибо, что поделились информацией! Объект появится на карте, как только мы проверим данные', {
                    duration: 9000,
                });
            } else {
                throw '7801'
            }
        }).catch(err => {
            const _err = err?.response?.data ? err.response.data : err?.message ? err.message : err
            toast.error(`Возникла ошибка: ${JSON.stringify(_err)}`, {
                duration: 3000,
            });
        })

        closeModal();
    }

    const showModal = () => {
        setIsModalShown(true)
    }

    const closeModal = () => {
        setIsModalShown(false)
    }

    return isModalShown ? (
        <div className="add-object-modal-wrapper bg-blur p-5 align-items-center justify-center d-flex">
            <BurgerCloseIcon onClick={closeModal} className="close-modal cursor-pointer" />

            <div className="modal-body flex-1 p-5 overflow-auto">
                <div className="text-center">
                    <h3 className="title is-4 mt-5">Заявка на добавление объекта</h3>

                    <p className="has-text-weight-bold">Наименование</p>
                    <input
                        required
                        name="name"
                        className="input is-small underline-input mb-4"
                        value={formValue.name}
                        onChange={handleChange}
                    />

                    <p className="has-text-weight-bold">Телефон</p>
                    <input
                        name="telephone"
                        className="input is-small underline-input mb-4"
                        value={formValue.telephone}
                        onChange={handleChange}
                    />

                    <p className="has-text-weight-bold">Адрес сайта или страничка</p>
                    <input
                        name="website"
                        className="input is-small underline-input mb-4"
                        value={formValue.website}
                        onChange={handleChange}
                    />

                    <div className="justify-center d-flex justify-center">
                        <p className="has-text-weight-bold mr-5">Адрес</p>
                        <a href="#" onClick={(e) => {
                            e.preventDefault(); closeModal()
                        }
                        }>показать на карте</a>
                    </div>
                    <input
                        required
                        name="address"
                        className="input is-small underline-input mb-5"
                        value={formValue.address}
                        onChange={handleChange}
                    />

                    <p className="mb-3">Комментарий</p>

                    <textarea
                        name="comment"
                        className="textarea underline-input underline-textarea mb-4"
                        placeholder=""
                        value={formValue.comment}
                        onChange={handleChange}
                    >
                    </textarea>

                    <div className="mb-5"></div>

                    <div className="justify-between d-flex mb-5">
                        <button className="button button-outlined" onClick={closeModal}>Отмена</button>
                        <button className="button button-main" onClick={sendObject}>Отправить заявку</button>
                    </div>
                </div>
            </div>

        </div>
    ) : null
}

export default AddObjectModal;
