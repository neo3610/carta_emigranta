import React from "react";

import { Outlet } from "react-router-dom";
import { Toaster } from 'react-hot-toast';

export default function RootLayout() {
  return (
    <section className="main-wrapper">
      <Outlet />
      <Toaster
        toastOptions={{
          success: {
            style: {
              background: 'green',
              color: 'white'
            },
          },
          error: {
            style: {
              background: 'red',
              color: 'white'
            },
          },
        }}
      />
    </section>
  );
}
