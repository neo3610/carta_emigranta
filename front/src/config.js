export const appVersion = "1.0.1"

export const citiesCenters = { warsaw: [52.22843, 21.00938] }

export const baseUrl = process.env.REACT_APP_BACKEND_API_URL;
