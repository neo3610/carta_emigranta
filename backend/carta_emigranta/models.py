from django.db import models
from django.core.exceptions import ValidationError


class Country(models.Model):
    name = models.CharField(max_length=199)
    map_x = models.FloatField(max_length=199, default=42.454511)
    map_y = models.FloatField(max_length=199, default=18.534411)
    def __str__(self):
        return str(self.name)

class City(models.Model):
    name = models.CharField(max_length=199)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    def __str__(self):
        return str(self.name)

class PointObjectOnMap(models.Model):
    super_id = models.IntegerField(default=0)
    name = models.CharField(max_length=199)
    address = models.CharField(max_length=199)
    telephone = models.CharField(max_length=199, default = 'no_phone')
    website = models.CharField(max_length=199, default = 'no_site')
    comment = models.CharField(max_length=199, default = 'no_comment')
    map_x = models.FloatField(max_length=199, default=42.454511)
    map_y = models.FloatField(max_length=199, default=18.534411)
    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True, blank=True)
    is_modereted = models.BooleanField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    #grade = models.CharField(max_length=199)
    def __str__(self):
        return str(self.name) + ' - ' + str(self.super_id) + str(self.city)


class SubCategory(models.Model):
    name = models.CharField(max_length=199)
    point_objects = models.ManyToManyField(PointObjectOnMap)
    def __str__(self):
        return str(self.name)

class Category(models.Model):
    name = models.CharField(max_length=199)
    sub_category=models.ManyToManyField(SubCategory)
    def __str__(self):
        return str(self.name)


class Announcement(models.Model):
    name = models.CharField(max_length=199)
    description = models.TextField(blank = True)
    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True, blank=True)
    sub_category = models.ForeignKey(SubCategory, on_delete=models.SET_NULL, null=True, blank=True)
