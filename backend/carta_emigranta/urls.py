from django.urls import path
from .views import CategoryView, PointObjectOnMaptView, CountryView, CityView, SearchObjectOnMaptView

urlpatterns = [
    path('objects_on_map/', CategoryView.as_view()),
    path('add_objects_on_map/', PointObjectOnMaptView.as_view()),
    path('search_objects/', SearchObjectOnMaptView.as_view()),
    path('cities/', CityView.as_view()),
    path('countries/', CountryView.as_view()),
]
