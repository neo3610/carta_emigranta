# Generated by Django 4.0.5 on 2022-11-27 16:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('carta_emigranta', '0011_alter_pointobjectonmap_is_modereted'),
    ]

    operations = [
        migrations.AlterField(
            model_name='pointobjectonmap',
            name='map_x',
            field=models.FloatField(default=42.454511, max_length=199),
        ),
        migrations.AlterField(
            model_name='pointobjectonmap',
            name='map_y',
            field=models.FloatField(default=18.534411, max_length=199),
        ),
    ]
