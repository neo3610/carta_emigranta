from django.shortcuts import render
from rest_framework import generics, filters


from .models import Category, PointObjectOnMap, Country, City, SubCategory
from .serializers import PointObjectSearchSerializer, CategorySerializer, PointObjectOnMapSerializer, CountrySerializer, CitySerializer

class CategoryView(generics.ListAPIView):
    serializer_class = CategorySerializer
    search_fields = ['sub_category__point_objects__comment','sub_category__point_objects__name']
    filter_backends = (filters.SearchFilter,)
    def get_queryset(self):
        city = self.request.query_params.get('city')
        country = self.request.query_params.get('country')
        if city is not None and country is not None:
            points_queryset = PointObjectOnMap.objects.filter(city__name=city).filter(city__country__name=country).all()
            points_queryset = points_queryset.filter(is_modereted=1).all()
            sub_category_queryset = SubCategory.objects.filter(point_objects__in=points_queryset)
            category_queryset = Category.objects.filter(sub_category__in=sub_category_queryset)
        elif city is None and country is not None:
            points_queryset = PointObjectOnMap.objects.filter(city__country__name=country).all()
            points_queryset = points_queryset.filter(is_modereted=1).all()
            sub_category_queryset = SubCategory.objects.filter(point_objects__in=points_queryset)
            category_queryset = Category.objects.filter(sub_category__in=sub_category_queryset)
        elif city is not None and country is None:
            points_queryset = PointObjectOnMap.objects.filter(city__name=city).all()
            points_queryset = points_queryset.filter(is_modereted=1).all()
            #sub_category_queryset = SubCategory.objects.filter(point_objects__in=points_queryset)  #заменил более длинной строкой ниже для проверки можно ли так делать =)
            category_queryset = Category.objects.filter(sub_category__point_objects__in=points_queryset)
        else:
            points_queryset = PointObjectOnMap.objects.filter(is_modereted=1).all()
            sub_category_queryset = SubCategory.objects.filter(point_objects__in=points_queryset)
            category_queryset = Category.objects.filter(sub_category__in=sub_category_queryset)

        return category_queryset


class PointObjectOnMaptView(generics.ListCreateAPIView):
    queryset = PointObjectOnMap.objects.all()
    serializer_class = PointObjectOnMapSerializer


class SearchObjectOnMaptView(generics.ListAPIView):
    search_fields = ['comment','name']
    filter_backends = (filters.SearchFilter,)
    def get_queryset(self):
        city = self.request.query_params.get('city')
        country = self.request.query_params.get('country')
        if city is not None and country is not None:
            points_queryset = PointObjectOnMap.objects.filter(city__name=city).filter(city__country__name=country).all()
        elif city is None and country is not None:
            points_queryset = PointObjectOnMap.objects.filter(city__country__name=country).all()
        elif city is not None and country is None:
            points_queryset = PointObjectOnMap.objects.filter(city__name=city).all()
        else:
            points_queryset = PointObjectOnMap.objects.all()
        return points_queryset
    serializer_class = PointObjectSearchSerializer



class CityView(generics.ListAPIView):
    queryset = City.objects.all()
    serializer_class = CitySerializer


class CountryView(generics.ListAPIView):
    queryset = Country.objects.all()
    serializer_class = CountrySerializer
