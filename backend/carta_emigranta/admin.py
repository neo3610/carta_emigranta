from django.contrib import admin
from carta_emigranta.models import PointObjectOnMap, SubCategory, Category, City, Country, Announcement
admin.site.register(PointObjectOnMap)
admin.site.register(SubCategory)
admin.site.register(Category)
admin.site.register(City)
admin.site.register(Country)
admin.site.register(Announcement)
# admin.site.register(Projects)
#
# @admin.register(Booking)
# class AuthorAdmin(admin.ModelAdmin):
#     #search_fields = ['seat']
#     list_display = ('date_is', 'seat')
#     # def save_model(self, request, obj, form, change):
#     #     #if obj.confirm == _('approved') :
#     #     #    obj.car.confirm =_('approved')
#     #     #    obj.car.save()
#     #     super().save_model(request, obj, form, change)
# #55.754945, 37.555492
