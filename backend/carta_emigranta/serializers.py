from rest_framework import serializers

from .models import Category, PointObjectOnMap, City, Country

class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = '__all__'
        depth = 3


class PointObjectOnMapSerializer(serializers.ModelSerializer):
    class Meta:
        model = PointObjectOnMap
        fields = '__all__'

class PointObjectSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = PointObjectOnMap
        fields = '__all__'

class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = '__all__'


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = '__all__'
