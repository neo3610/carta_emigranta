from .models import Category, PointObjectOnMap, Country, City, SubCategory
from rest_framework.test import APITestCase, APIClient

from django.contrib.auth import get_user_model

from django.test import TestCase
from django.forms.models import model_to_dict

import warnings
warnings.filterwarnings(
    "ignore", message="No directory at", module="whitenoise.base")
warnings.filterwarnings("ignore", message="staticfiles")

clnt = APIClient()
User = get_user_model()

class MapPointsTestCase(TestCase):
    maxDiff = None
    testuserId = None
    serialized_rollback = True

    def test_all_endpoints(self):
        country = Country.objects.create(
            name='SomeCountry',
            map_x=42.00,
            map_y=18.00,
        )

        city = City.objects.create(
            name='SomeCity',
            country=country,
        )

        response = clnt.get('/api/objects_on_map/').json()
        self.assertEqual(response, [])

        response = clnt.post('/api/add_objects_on_map/', {
            "somefield": "test3",
        }).json()

        self.assertEqual(response, {
            'name': ['This field is required.'],
            'address': ['This field is required.']
        })

        response = clnt.post('/api/add_objects_on_map/', {
            "name": "Бровиресничка",
            "address": "ул. Пушкина, д. Колотушкина",
            "map_x": 42.77,
            "map_y": 18.99,
            "city": city.id,
            "is_modereted": False
        }).json()

        del response['updated_at']
        del response['created_at']

        self.assertEqual(
            response,
            {
                'id': 1,
                'super_id': 0,
                'name': 'Бровиресничка',
                'address': 'ул. Пушкина, д. Колотушкина',
                'telephone': 'no_phone',
                'website': 'no_site',
                'comment': 'no_comment',
                'map_x': 42.77,
                'map_y': 18.99,
                'is_modereted': False,
                'city': 1
            })

        response = clnt.post('/api/add_objects_on_map/', {
            "name": "Бровиресничка2",
            "address": "ул. Пушкина2, д. Колотушкина2",
            "map_x": 42.78,
            "map_y": 18.98,
            "city": city.id
        }).json()

        subCategory = SubCategory.objects.create(
            name='SubCategory1',
        )

        pointObjectsAll = PointObjectOnMap.objects.all()
        subCategory.point_objects.set(pointObjectsAll)

        category = Category.objects.create(
            name='Category1',
        )

        category.sub_category.set([subCategory])

        response = clnt.get('/api/objects_on_map/').json()

        self.assertEqual(response, [])

        for object in pointObjectsAll:
            object.is_modereted = True
            object.save()

        response = clnt.get('/api/objects_on_map/').json()

        for object in response:
            for obj in object['sub_category']:
                for obj2 in obj['point_objects']:
                    del obj2['updated_at']
                    del obj2['created_at']

        self.assertEqual(response, [
            {
                'id': 1,
                'name': 'Category1',
                'sub_category': [
                    {
                        'id': 1,
                        'name': 'SubCategory1', 'point_objects': [
                            {
                                'id': 1,
                                'super_id': 0,
                                'name': 'Бровиресничка',
                                'address': 'ул. Пушкина, д. Колотушкина',
                                'telephone': 'no_phone',
                                'website': 'no_site',
                                'comment': 'no_comment',
                                'map_x': 42.77,
                                'map_y': 18.99,
                                'is_modereted': True,
                                'city': {
                                    'id': 1,
                                    'name': 'SomeCity',
                                    'country': 1
                                }
                            },
                            {
                                'id': 2,
                                'super_id': 0,
                                'name': 'Бровиресничка2',
                                'address': 'ул. Пушкина2, д. Колотушкина2',
                                'telephone': 'no_phone',
                                'website': 'no_site',
                                'comment': 'no_comment',
                                'map_x': 42.78,
                                'map_y': 18.98,
                                'is_modereted': True, 'city': {
                                    'id': 1,
                                    'name': 'SomeCity',
                                    'country': 1
                                }
                            }
                        ]
                    }
                ]
            }
        ]
        )
