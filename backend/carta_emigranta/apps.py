from django.apps import AppConfig


class CartaEmigrantaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'carta_emigranta'
