# carta_emigranta

## to run Backend
```
cd backend
python3 -m venv venv
source venv/bin/activate
# pip3 freeze > requirements.txt
pip3 install -r requirements.txt
cp db.default.sqlite3 db.sqlite3
python3 manage.py runserver
```

## to run frontend
 - install node 16.16
 - install npm 8.11

```
node -v
npm -v
cp example.env .env
npm i
npm start
```

## admin url:
http://127.0.0.1:8000/admin/

## admin pass and login:
carta_emigranta
carta_emigranta

## endpoints
http://127.0.0.1:8000/api/objects_on_map/
